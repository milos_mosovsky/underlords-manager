local _, UDM = ...

Underlords_Manager = LibStub("AceAddon-3.0"):NewAddon("Underlords_Manager", "AceConsole-3.0")

function Underlords_Manager:OnInitialize()
    -- Called when the addon is loaded
end

function Underlords_Manager:OnEnable()
    -- Called when the addon is enabled
    self:Print("Hello World!")
    self:RegisterChatCommand("udm", function(args) Underlords_Manager:HandleChatCommand(args) end )
end

function Underlords_Manager:OnDisable()
    -- Called when the addon is disabled
end

function Underlords_Manager:Debug(addon, text)
    self:Print(addon .. ": "..text)
end

UDM.Debug = function(addon, text) 
    Underlords_Manager:Debug(addon, text)
end


function Underlords_Manager:HandleChatCommand(command)
    local command, args = self:GetArgs(command, 2)
    if command == "loot" then
        Underlords_Manager_AutoLoot:Run(args)
        return
    end

    if command == nil then
        self:Print("Unkown command")
        return
    end

    self:Print("Unkown command: " .. command)
end