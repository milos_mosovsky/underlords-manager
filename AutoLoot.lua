local _, UDM = ...
local MODULE = "Underlords_Manager_AutoLoot"

Underlords_Manager_AutoLoot = LibStub("AceAddon-3.0"):NewAddon(MODULE, "AceConsole-3.0")


function Underlords_Manager_AutoLoot:OnEnable()
    -- Called when the addon is enabled
    -- self:Debug("test")
    -- self:Warning("test")
end

function Underlords_Manager_AutoLoot:Run(itemLink)
    self:Warning("Loot: " .. itemLink)
end

function Underlords_Manager_AutoLoot:Warning(text)
    UIErrorsFrame:AddMessage(text, 0.78, 0.25, 0.05)
end

function Underlords_Manager_AutoLoot:Debug(text)
    UDM.Debug(MODULE, text)
end